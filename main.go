package main

import (
	"fmt"
	"os/exec"
	"sync"
)

var wg = sync.WaitGroup{}

func main() {
	// Array of GitHub repository URLs
	repos := []string{
		// "gsheet-api-svelte-5",
		"phoenix-cheatsheet",
		"phoenixadmissioncare.com",
		"cse115.6-project-1",
		"IUP-GUI-C",
		"fastbd.net",
		"routine-generator",
		"lyghtstone",
		"bongo-bonik",
		"timus-c",
		"nvim-settings",
		"sveltekit-pwa-ssg-template",
		"vscode-keybindings-for-sublime",
		"Sveltekit-Vocabulary-Question-Generator",
		"katex-output",
		"learning-platform-react-client",
		"learning-platform-react-server",
		"buzz-quizzers-react-router",
		"best-five-player-vanillaJS",
		"bookfee-client",
		"fighting-faris-react",
		"world-cup-website",
		"stylish-font-in-display-picture",
		"news-portal-vanillaJS",
		"EdTech-Tailwind",
		"leader-board-css3",
		"panda-commerce-bootstrap",
		"ranga-store-vanga-custom-search-functionality",
		"my-first-git-practice-repo",
		"developer-portfolio",

		"donate-website",
		"debugging-practice",
		"react-practice",
		"webpage-shape-builder-with-12-parameter",
		"color-picker-and-theory-kivy",
		"is_form",
		"Link-Based-Routine",
		// Add more repository URLs here
	}

	// Iterate over the repositories
	for _, repoName := range repos {
		wg.Add(1)
		go goGet(repoName)
	}

	wg.Wait()
}

func goGet(repoName string) {

	repoURL := fmt.Sprintf("https://github.com/rafiul-hossain-riday/%v.git", repoName)
	// Download the repository
	cmd := exec.Command("git", "clone", repoURL)
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error cloning repository:", err)

		wg.Done()
		return
	}

	// Create a new GitLab repository
	cmd = exec.Command("glab", "repo", "create", repoName, "--public")
	cmd.Dir = repoName
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error creating GitLab repository:", err, out)

		wg.Done()
		return
	}

	// push the repository to GitLab
	cmdList := [][]string{
		{"git", "remote", "rename", "origin", "old-origin"},
		{"git", "remote", "add", "origin", fmt.Sprintf("https://gitlab.com/rafi.riday/%v.git", repoName)},
		{"git", "push", "--set-upstream", "origin", "--all"},
		{"git", "push", "--set-upstream", "origin", "--tags"},
	}
	for idx, c := range cmdList {
		cmd = exec.Command(c[0], c[1:]...)
		cmd.Dir = repoName
		out, err = cmd.CombinedOutput()
		if err != nil {
			fmt.Println(append(c, []string{}...))
			fmt.Println("Error executing command", (idx + 1), ":", err, out)
		}
	}
	fmt.Println("Repository", repoName, "cloned and pushed to GitLab")

	// Delete the local repository
	cmd = exec.Command("cmd", "/c", "rd", "/s", "/q", repoName)
	err = cmd.Run()
	if err != nil {
		fmt.Println("Error deleting local repository:", err)
	}

	wg.Done()
}
